from random import randint

name = input("Hi! What is your name?")

for i in range(5):
    i += 1
    year = randint(1924, 2004)
    month = randint(1, 12)

    answer = input(f'Guess {i}: {name} were you born in {month} / {year} \n yes or no?')

    if answer == "no" and i < 5:
        print("Drat! Lemme try again!")
    if answer == "no" and i == 5:
        print("I have other things to do. Good bye.")
    if answer == "yes":
        print("I knew it!")
        exit()
